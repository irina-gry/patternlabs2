package state2;

public class NoCoinState implements State {
    VendingMachine vendingMachine;
 
    public NoCoinState(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }
 
	public void insertCoin() {
		System.out.println("�� ��������� ������");
		vendingMachine.setState(vendingMachine.getHasCoinState());
	}
 
	public void ejectCoin() {
		System.out.println("�� �� ��������� ������");
	}
 
	public void pushSelectButton() {
		System.out.println("�� ��������� ������� �����, �� �������� ������");
	 }
 
	public void dispense() {
		System.out.println("������ ���������� ���������� ������");
	} 
 
	public String toString() {
		return "�������� ������";
	}
}
