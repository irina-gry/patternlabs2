package state2;

import java.util.Random;

public class HasCoinState implements State {
	VendingMachine vendingMachine;
 
	public HasCoinState(VendingMachine vendingMachine) {
		this.vendingMachine = vendingMachine;
	}
  
	public void insertCoin() {
		System.out.println("������ ��������� ������ ����� ������");
	}
 
	public void ejectCoin() {
		System.out.println("������ ����������");
		vendingMachine.setState(vendingMachine.getNoCoinState());
	}
 
	public void pushSelectButton() {
		System.out.println("����� ������...");
		vendingMachine.setState(vendingMachine.getSoldState());
	}

    public void dispense() {
        System.out.println("����� �� �����");
    }
 
	public String toString() {
		return "�������� ������ ������";
	}
}
