package state2;

public interface State {
 
	public void insertCoin();
	public void ejectCoin();
	public void pushSelectButton();
	public void dispense();
}
