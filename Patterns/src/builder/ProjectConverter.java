package builder;

import java.util.ArrayList;

// ���������
public class ProjectConverter extends NodeConverter 
		 {
	private Project project;
	public ProjectConverter(){
		this.project=new Project();
	}
	@Override
	public void readProject(String projectname) {
		project.setProjectName(projectname);
	}
	public void readSupervisor(String supervisorName){
		project.setProjectSupervisor(supervisorName);
	}
	public void readEmail(String email){
		project.setProjectEmail(email);
	}
	public void readDescription(String description){
		project.setProjectDescription(description);
	}
	public void readDept(String dept){
		project.setProjectDept(dept);
	}
	public void readGroup(String group){
		project.setProjectGroup(group);
	}
	public void displayProject()
	{
		System.out.println("----------Project----------");
		System.out.println("Deptno:"+project.getProjectDept());
		System.out.println("Group:"+project.getProjectGroup());
		System.out.println("ProjectName:"+project.getProjectName());
		System.out.println("Email:      "+project.getProjectEmail());
		System.out.println("Supervisor: "+project.getProjectSupervisor());
		System.out.println("Description:"+project.getProjectDescription());
	}
}
