package builder;

public class Project implements ProjectPlan {
	private String projectName;
	private String projectEmail;
	private String projectSupervisor;
	private String projectDescription;
	private String projectDept;
	private String projectGroup;
	@Override
	public void setProjectName(String projectname) {
		// TODO Auto-generated method stub
		this.projectName=projectname;
	}

	@Override
	public void setProjectEmail(String email) {
		// TODO Auto-generated method stub
		this.projectEmail=email;
	}

	@Override
	public void setProjectSupervisor(String supervisor) {
		// TODO Auto-generated method stub
		this.projectSupervisor=supervisor;
	}

	@Override
	public void setProjectDescription(String description) {
		// TODO Auto-generated method stub
		this.projectDescription=description;
	}
	@Override
	public void setProjectDept(String dept) {
		// TODO Auto-generated method stub
		this.projectDept=dept;
	}
	@Override
	public void setProjectGroup (String group) {
		this.projectGroup=group;
	}
	public String getProjectName(){
		return this.projectName;
	}
	public String getProjectEmail(){
		return this.projectEmail;
	}
	public String getProjectSupervisor(){
		return this.projectSupervisor;
	}
	public String getProjectDescription(){
		return this.projectDescription;
	}
	public String getProjectDept(){
		return this.projectDept;
	}
	public String getProjectGroup(){
		return this.projectGroup;
	}
}
