package builder;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class DocReader {

	private NodeConverter nodeconverter;
	private String fileName;

	public DocReader(NodeConverter nodeconverter, String fileName) {
		this.nodeconverter=nodeconverter;
		this.fileName=fileName;
	}

	public void build() {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			
			Document document = documentBuilder.parse(fileName);
			DOMNodes projectNodes = new DOMNodes(document.getElementsByTagName("project"));
			for (Node node : projectNodes) {
				nodeconverter.readProject(getNodeAttribute(node, "title"));
				nodeconverter.readEmail(getNodeAttribute(node,"tutormail"));
				nodeconverter.readSupervisor(getNodeAttribute(node, "tutorname"));
				nodeconverter.readDept(getNodeAttribute(node, "dept"));
				nodeconverter.readGroup(getNodeAttribute(node, "group"));
				nodeconverter.readDescription(node.getFirstChild().getNodeValue());
				nodeconverter.displayProject();
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getNodeAttribute(Node node, String attributeName) {
		return node.getAttributes().getNamedItem(attributeName).getNodeValue();
	}
	
}
