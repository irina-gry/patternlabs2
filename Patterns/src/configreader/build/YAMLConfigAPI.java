package configreader.build;

import configreader.yaml.YamlBuildConfigurationReader;
import configreader.build.BuildConfigAdapter;
import configreader.build.Config;
import configreader.build.ConfigReaderAPI;

public class YAMLConfigAPI implements ConfigReaderAPI {

	@Override
	public Config getConfig(String filename) {
		// TODO Auto-generated method stub
		Config config = new BuildConfigAdapter(new YamlBuildConfigurationReader("build.yaml").getBuildConfig());
		
		System.out.println("Yaml build config: "+config);
		
		return config;
	}

}
