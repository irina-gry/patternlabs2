package configreader.build;

import configreader.build.ConfigAPIFactory;
import configreader.build.ConfigReaderAPI;
import configreader.build.YAMLConfigAPI;

public class YAMLConfigAPIFactory extends ConfigAPIFactory{

	public ConfigReaderAPI getAPI (){
		return new YAMLConfigAPI();
	}

}