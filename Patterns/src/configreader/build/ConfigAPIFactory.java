package configreader.build;

import configreader.build.ConfigReaderAPI;

public abstract class ConfigAPIFactory {
	public abstract ConfigReaderAPI getAPI();
}
