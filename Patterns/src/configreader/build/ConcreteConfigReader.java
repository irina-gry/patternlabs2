package configreader.build;

import configreader.build.ConfigReader;
import configreader.build.ConfigReaderAPI;

public class ConcreteConfigReader extends ConfigReader{
	private String filename;

	public ConcreteConfigReader(String filename,ConfigReaderAPI configReaderAPI) {
    	super(configReaderAPI);
    	this.filename = filename;
	}
	@Override
	public Config getConfig() {
		// TODO Auto-generated method stub
		return configReaderAPI.getConfig(filename);
	}

}
