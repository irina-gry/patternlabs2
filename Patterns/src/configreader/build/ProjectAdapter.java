package configreader.build;

import java.util.List;

import configreader.build.Config;
import configreader.domain.Project;
import configreader.domain.Property;
import configreader.domain.Target;

public class ProjectAdapter implements Config {
		private Project project;
		
		public ProjectAdapter(Project project){
			this.project = project;
		}
		
		@Override
		public Project getProject() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setProject(Project project) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String getDescription() {
			// TODO Auto-generated method stub
			return this.project.getDescription();
		}

		@Override
		public void setDescription(String description) {
			// TODO Auto-generated method stub
			this.project.setDescription(description);
		}

		@Override
		public String getSrc() {
			// TODO Auto-generated method stub
			return this.project.getSrc();
		}

		@Override
		public void setSrc(String src) {
			// TODO Auto-generated method stub
			this.project.setSrc(src);
		}

		@Override
		public String getBuild() {
			// TODO Auto-generated method stub
			return this.project.getBuild();
		}

		@Override
		public void setBuild(String build) {
			// TODO Auto-generated method stub
			this.project.setBuild(build);
		}

		@Override
		public String getDist() {
			// TODO Auto-generated method stub
			return this.project.getDist();
		}

		@Override
		public void setDist(String dist) {
			// TODO Auto-generated method stub
			this.project.setDist(dist);
		}

		@Override
		public List<Target> getTargets() {
			// TODO Auto-generated method stub
			return this.project.getTargets();
		}

		@Override
		public void setTargets(List<Target> targets) {
			// TODO Auto-generated method stub
			this.project.setTargets(targets);
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return this.project.getName();
		}

		@Override
		public void setName(String name) {
			// TODO Auto-generated method stub
			this.project.setName(name);
		}

		@Override
		public Target getDefaultTarget() {
			// TODO Auto-generated method stub
			return this.project.getDefaultTarget();
		}

		@Override
		public void setDefaultTarget(Target defaultTarget) {
			// TODO Auto-generated method stub
			this.project.setDefaultTarget(defaultTarget);
		}

		@Override
		public String getBasedir() {
			// TODO Auto-generated method stub
			return this.project.getBasedir();
		}

		@Override
		public void setBasedir(String basedir) {
			// TODO Auto-generated method stub
			this.project.setBasedir(basedir);
		}

		@Override
		public List<Property> getProperties() {
			// TODO Auto-generated method stub
			return this.project.getProperties();
		}

		@Override
		public void setProperties(List<Property> properties) {
			// TODO Auto-generated method stub
			this.project.setProperties(properties);
		}
		
		@Override
		public String toString() {
			return project.toString();
		}
	}