package configreader.build;

import java.util.List;

import configreader.domain.Project;
import configreader.domain.Property;
import configreader.domain.Target;

public interface Config {
  
	public Project getProject();

	public void setProject(Project project);
	
	public String getDescription() ;

	public void setDescription(String description) ;

	public String getSrc() ;

	public void setSrc(String src);

	public String getBuild();

	public void setBuild(String build) ;

	public String getDist() ;

	public void setDist(String dist) ;

	public List<Target> getTargets() ;

	public void setTargets(List<Target> targets) ;

	public String getName() ;

	public void setName(String name);

	public Target getDefaultTarget() ;

	public void setDefaultTarget(Target defaultTarget) ;

	public String getBasedir() ;

	public void setBasedir(String basedir) ;

	public List<Property> getProperties() ;

	public void setProperties(List<Property> properties);
}