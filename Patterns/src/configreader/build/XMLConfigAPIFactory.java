package configreader.build;

import configreader.build.ConfigAPIFactory;

public class XMLConfigAPIFactory extends ConfigAPIFactory{
	
	public ConfigReaderAPI getAPI(){
		return new XMLConfigAPI();
	}

}
